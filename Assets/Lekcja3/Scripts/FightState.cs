﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightState : BaseState<MachineState>
{
    public override void InitState(MachineState controller)
    {
        Debug.Log("FightState :: InitState");
    }

    public override void UpdateState()
    {
        Debug.Log("FightState :: UpdateState");
    }

    public override void DeinitState(MachineState controller)
    {        
        Debug.Log("FightState :: DeinitState");
    }    
}
