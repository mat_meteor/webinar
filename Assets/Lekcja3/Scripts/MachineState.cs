﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineState : BaseBrain<MachineState>
{
    private void Start()
    {
        StartFigthState();
    }

    private void Update()
    {
        UpdateState();
    }

    public override void ChangeState(BaseState<MachineState> newState)
    {
        currentState?.DeinitState(this);
        currentState = newState;
        currentState?.InitState(this);
    }

    public override void UpdateState()
    {
        currentState?.UpdateState();
    }

    [ContextMenu("StartFightState")]
    public void StartFigthState()
    {
        ChangeState(new FightState());
    }

    [ContextMenu("StartWaitState")]
    public void StartWaitState()
    {
        ChangeState(new WaitState());
    }
}
