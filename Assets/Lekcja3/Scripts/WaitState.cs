﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitState : BaseState<MachineState>
{
    public override void InitState(MachineState controller)
    {
        Debug.Log("WaitState :: InitState");
    }

    public override void UpdateState()
    {
        Debug.Log("WaitState :: UpdateState");
    }

    public override void DeinitState(MachineState controller)
    {
        Debug.Log("WaitState :: DeinitState");
    }
}
