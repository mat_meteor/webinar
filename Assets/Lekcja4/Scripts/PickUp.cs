﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public WeaponInfo info;
    public Player player;

    private void OnMouseDown()
    {
        player.PickUpWeapon(info);
    }
}
