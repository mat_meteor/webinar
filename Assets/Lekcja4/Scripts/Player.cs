﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Animator playerAnimator;
    public Transform weaponParent;
    public GameObject currentWeaponObject;

    [Header("Stats")]
    public float attackDelay;
    public float damage;
    public float range;

    private float attackStartTime;

    public void PickUpWeapon(WeaponInfo info)
    {
        if(currentWeaponObject != null)
        {
            Destroy(currentWeaponObject);
        }

        currentWeaponObject = Instantiate(info.weaponPrefab);
        currentWeaponObject.transform.parent = weaponParent;
        currentWeaponObject.transform.localPosition = Vector3.zero;

        attackDelay = info.attackDelay;
        damage = info.attackDamage;
        range = info.attackRange;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && Time.time > attackStartTime + attackDelay)
        {
            Attack();
        }
    }

    public void Attack()
    {
        attackStartTime = Time.time;
        playerAnimator.SetTrigger("Attack");
    }
}
