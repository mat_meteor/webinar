﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    public Rigidbody shellRb;

    private void Update()
    {
        Debug.Log(shellRb.velocity.normalized);
        transform.forward = shellRb.velocity.normalized;
    }
}
