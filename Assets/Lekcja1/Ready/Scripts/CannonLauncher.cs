﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonLauncher : MonoBehaviour
{
    public Rigidbody ballPrefab;
    public Transform launchPosition;
    public float force;
    public float destroyDelay;

    private void OnMouseDown()
    {
        Rigidbody cannonBall = Instantiate(ballPrefab, launchPosition.transform.position, launchPosition.transform.rotation);
        cannonBall.AddForce(launchPosition.forward * force, ForceMode.Impulse);
        Destroy(cannonBall.gameObject, destroyDelay);
    }
}
