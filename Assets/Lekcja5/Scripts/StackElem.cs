﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackElem : MonoBehaviour
{
    public LineRenderer lineRend;
    public Rigidbody stackRigidbody;
    public Vector3 leftPosition;
    public Vector3 rightPosition;

    public float currentLeftBorder;
    public float currentRightBorder;
    public float length;

    public bool proceedMove;

    public void Init(StackElem previousStackElem, Color color)
    {
        leftPosition = previousStackElem.leftPosition;
        rightPosition = previousStackElem.rightPosition;

        lineRend.positionCount = 2;
        lineRend.SetPosition(0, leftPosition);
        lineRend.SetPosition(1, rightPosition);
        proceedMove = true;
        SetColor(color);

        length = (leftPosition - rightPosition).magnitude;
        transform.position = previousStackElem.transform.position + new Vector3(0f, 1f, 0f);
    }

    public void SetColor(Color color)
    {
        lineRend.startColor = color;
        lineRend.endColor = color;
    }

    public void UpdateStack()
    {
        lineRend.positionCount = 2;
        lineRend.SetPosition(0, leftPosition);
        lineRend.SetPosition(1, rightPosition);
        currentLeftBorder = this.transform.position.x + leftPosition.x;
        currentRightBorder = this.transform.position.x + rightPosition.x;
    }

    public void ActivateRigidbody()
    {
        stackRigidbody.isKinematic = false;
    }

    void Update()
    {
        currentLeftBorder = this.transform.position.x + leftPosition.x;
        currentRightBorder = this.transform.position.x + rightPosition.x;
        FloatPosition();
    }

    public void FloatPosition()
    {
        if (proceedMove)
        {
            var pos = transform.position;
            pos.x = Mathf.Lerp(-2f, 2f, (Mathf.Sin(Time.time) + 1) / 2f);
            transform.position = pos;
        }
    }
}
