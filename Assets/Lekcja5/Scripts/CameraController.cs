﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CameraController
{
    public Camera currentCamera;
    public float yValue;
    public float speed;
    public float additionalHeight;

    public void Init(float startY)
    {
        yValue = startY;
    }

    public void UpdateCamera()
    {
        var pos = currentCamera.transform.position;
        pos.y = Mathf.Lerp(currentCamera.transform.position.y, yValue + additionalHeight, Time.deltaTime * speed);
        currentCamera.transform.position = pos;
    }

    public void UpdatePosition(float yValue)
    {
        this.yValue = yValue;
    }

}
