﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameView : MonoBehaviour
{
    public GameObject summaryPanel;

    public TextMeshProUGUI scoreCounter;
    public TextMeshProUGUI finalScore;

    public void ResetScore()
    {
        scoreCounter.text = "0";
        finalScore.text = "0";
        summaryPanel.SetActive(false);
    }

    public void UpdateCurrentScore(float score)
    {
        scoreCounter.text = score.ToString();
    }

    public void UpdateFinalScore(float score)
    {
        finalScore.text = score.ToString();
    }

    public void ShowFinalWindow()
    {
        summaryPanel.SetActive(true);
    }
}
