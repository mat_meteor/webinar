﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StackScript : MonoBehaviour
{
    public StackElem previousStack;
    public StackElem currentStack;
    public StackElem stackPrefab;
    private List<StackElem> elements;

    public ColorPairs colorPairs;
    public CameraController cameraController;
    public GameView gameView;

    public Transform stackParent;
    public SpriteRenderer testSprite;

    public float threshold;
    public float speed;

    public float currentScore;

    public void StartGame()
    {
        if (elements != null)
        {
            for (int i = elements.Count - 1; i > 0; i--)
            {
                Destroy(elements[i].gameObject);
            }
        }

        elements = new List<StackElem>();
        gameView.ResetScore();
        colorPairs.Init();
        previousStack = Instantiate(stackPrefab, Vector3.zero, Quaternion.identity, stackParent);
        previousStack.SetColor(Color.gray);
        previousStack.UpdateStack();

        currentStack = Instantiate(stackPrefab, Vector3.zero, Quaternion.identity, stackParent);
        currentStack.Init(previousStack, colorPairs.GetCurrentColor());
        cameraController.Init(previousStack.transform.position.y);
        currentScore = 0;

        elements.Add(previousStack);
        elements.Add(currentStack);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StackCurrent();
        }

        cameraController?.UpdateCamera();
    }

    [ContextMenu("Stack")]
    public void StackCurrent()
    {
        var leftDiff = previousStack.currentLeftBorder - currentStack.currentLeftBorder;
        currentStack.enabled = false;

        if(Mathf.Abs(leftDiff) > currentStack.length)
        {
            gameView.ShowFinalWindow();
            currentStack.enabled = false;
            currentStack.ActivateRigidbody();
            return;
        }

        currentStack.transform.position = previousStack.transform.position + new Vector3(0f, 1f, 0f);

        if (leftDiff > threshold)
        {
            CreateLeftOver(currentStack.leftPosition - new Vector3(leftDiff, 0), currentStack.leftPosition);
            currentStack.leftPosition = previousStack.leftPosition;
            currentStack.rightPosition.x = previousStack.rightPosition.x - leftDiff;
            currentStack.UpdateStack();
        } 
        else if (leftDiff < threshold * -1f)
        {
            CreateLeftOver(currentStack.rightPosition, currentStack.rightPosition - new Vector3(leftDiff, 0));
            currentStack.rightPosition = previousStack.rightPosition;
            currentStack.leftPosition.x = previousStack.leftPosition.x - leftDiff;
            currentStack.UpdateStack();
        }

        CreateAntoher();
    }   

    public void CreateLeftOver(Vector3 leftPosition, Vector3 rightPosition)
    {
        StackElem newStack = Instantiate(stackPrefab);
        newStack.SetColor(currentStack.lineRend.startColor);
        newStack.transform.position = currentStack.transform.position;
        newStack.leftPosition = leftPosition;
        newStack.rightPosition = rightPosition;
        newStack.UpdateStack();
        newStack.ActivateRigidbody();
    }

    public void CreateAntoher()
    {
        previousStack = currentStack;
        currentStack = Instantiate(stackPrefab);
        currentStack.Init(previousStack, colorPairs.GetCurrentColor());
        cameraController.UpdatePosition(previousStack.transform.position.y);
        colorPairs.PickNextBlock();
        currentScore++;
        gameView.UpdateCurrentScore(currentScore);
        elements.Add(currentStack);
    }
   
    [ContextMenu("TestColor")]
    public void TestColorSwitching()
    {
        testSprite.color = colorPairs.GetCurrentColor();
    }

    public void QuitGame()
    {
        Application.Quit();
    }    
}

[System.Serializable]
public class ColorPairs
{
    public List<Color> availbeColors;
    public List<Color> currentColors; 

    public Color startColor;
    public Color targetColor;

    public float blocksCount;
    public float currentBlocks;

    public void Init()
    {        
        currentBlocks = 0;
        InitColors();
        PickColor(out startColor);
        PickColor(out targetColor);
    }

    public void PickColor(out Color color)
    {
        if(currentColors.Count == 0)
        {
            InitColors();
        }

        color = currentColors[Random.Range(0, currentColors.Count)];
        currentColors.Remove(color);
    }

    public void InitColors()
    {
        currentColors = new List<Color>();
        currentColors.AddRange(availbeColors);
    }

    public Color GetCurrentColor()
    {
        var value = currentBlocks / blocksCount;
        Debug.Log("GetCurrent");
        return Color.Lerp(startColor, targetColor, value);
    }

    public void PickNextBlock()
    {
        currentBlocks++;
        Debug.Log("NexrBlock");
        if (currentBlocks >= blocksCount)
        {
            startColor = targetColor;
            currentBlocks = 0;
            //PickColor(out targetColor);
        }
    }
}
