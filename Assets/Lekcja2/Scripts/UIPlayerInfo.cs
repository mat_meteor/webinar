﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIPlayerInfo : MonoBehaviour
{
    public Slider slider;
    public Image healthBar;

    public TextMeshProUGUI attackText;
    public TextMeshProUGUI defeceText;

    public int currentAttack;
    public int currentDefence;

    public void Start()
    {
        SetCurrentStatistics();
        slider.onValueChanged.AddListener(SliderValueChanged);
    }

    public void AddAttack()
    {
        currentAttack++;
        SetCurrentStatistics();
    }

    public void AddDefence()
    {
        currentDefence++;
        SetCurrentStatistics();
    }

    public void SetCurrentStatistics()
    {
        attackText.text = currentAttack.ToString();
        defeceText.text = currentDefence.ToString();
    }

    public void SliderValueChanged(float value)
    {
        healthBar.fillAmount = value;
    }
    
}
